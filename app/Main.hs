module Main where

import System.Environment

import Apiary (renderApiFile)

main :: IO ()
main = do
  args <- getArgs
  case args of
    [src, dest] -> renderApiFile src dest
    _ -> putStrLn "usage: apiary <src> <dest>"