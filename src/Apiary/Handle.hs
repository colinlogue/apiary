module Apiary.Handle
  ( handle
  , Handler(..)
  , mkHandler
  , exact
  , value
  , eof
  ) where

import Data.Foldable
import Data.Monoid
import Data.Typeable
import Flow
import Network.HTTP.Types (parseMethod)
import Network.Wai (requestMethod)
import Text.Read (readMaybe)

import Apiary.Servable (Servable(..))
import Apiary.Types
import Types
import Utils



newtype Handler a = Handler { getHandler :: (StdMethod, Path) -> a }

instance Servable a => Semigroup (Handler a) where
  g <> h = Handler (\x ->
    if (getHandler g) x == invalid
      then getHandler h $ x
      else getHandler g $ x)

instance Servable a => Monoid (Handler a) where
  mempty = Handler (\_ -> invalid)

instance Functor Handler where
  -- fmap :: (a -> b) -> Handler a -> Handler b
  fmap h (Handler g) = Handler (h . g)

instance Applicative Handler where
  pure x = Handler (\_ -> x)
  Handler g <*> Handler h = Handler (\x -> (g x) (h x))

handle :: (Foldable t, Servable a) => t (Handler a) -> Request -> a
handle hs req =
  case parseMethod $ requestMethod req of
    Ok meth -> (getHandler $ fold hs) (meth, getPath req)
    Err _ -> invalid


mkHandler :: Servable b => StdMethod -> a -> Match a b -> Handler b
mkHandler meth ctor match = Handler (
  \(m, p) ->
    if m == meth
      then case justFst $ match $ Just (ctor, p) of
        Just x -> x
        Nothing -> invalid
      else invalid)

type Match a b = Maybe (a, Path) -> Maybe (b, Path)

exact :: String -> Match a a
exact s = (\x ->
  case x of
    Just (y, s:zs) -> Just (y, zs)
    _ -> Nothing)

eof :: Match a a
eof (Just (x, [])) = Just (x, [])
eof _ = Nothing

value :: Read a => Match (a -> b) b
value (Just (t, y:ys)) =
  case readMaybe y of
    Just val -> Just (t val, ys)
    Nothing -> Nothing
value _ = Nothing

match :: StdMethod -> PathTemplate -> Handler a
match = undefined