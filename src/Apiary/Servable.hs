module Apiary.Servable where

--import Network.HTTP.Types.Method

import Types

class Eq a => Servable a where
  -- | Action for any request that doesn't match a rule.
  invalid :: a
  serve :: a -> IO Response
