module Apiary.Render where


import Data.List
import Flow

import Apiary.Types
import Utils


class Render a where
  -- | Defines how an element should be rendered in the
  -- | output .hs file.
  render :: a -> String


instance Render Constructor where
  render x = ctorName x ++ " " ++ ((intercalate " ") $ args x)


instance Render ApiRule where
  render (ApiRule m p c) =
    "let" ++ newline ++ (indent $
      "match =" ++ newline ++ (indent $
        renderMatch)) ++ newline ++
    "in" ++ newline ++ intercalate " "
    ["mkHandler", show m, ctorName c, "match"]
    |> indent where

      renderMatch :: String
      renderMatch =
        (intercalate (" .>" ++ newline) $ renderPath p)
          where
            renderPath :: PathTemplate -> [String]
            renderPath EmptyPath = ["eof"]
            renderPath (ValuePath _ xs) = "value":renderPath xs
            renderPath (ExactPath x xs) =
              ("exact \"" ++ x ++ "\"") : renderPath xs


instance Render API where
  render (API x) =
    -- render header
    "module API where" ++ newline ++ newline ++
    --"import Data.Typeable" ++ newline ++
    "import Flow" ++ newline ++
    "import Network.HTTP.Types.Method" ++ newline ++
    "import Apiary.Handle" ++ newline ++
    "import Types" ++ newline ++ newline ++
    -- render data type
    "data Action" ++ newline ++
    "  = " ++ intercalate "\n  | " (getCtorStrs x) ++ newline ++
    "  | InvalidAction" ++ newline ++
    --"  deriving (Typeable)"
    "  deriving (Show, Eq)"
    ++ newline ++ newline ++
    "handlers = [\n" ++
    (intercalate ",\n" $ fmap render x) ++
    "\n  ]"
      where
        getCtorStrs :: [ApiRule] -> [String]
        getCtorStrs = fmap (render . ctor)

-- | If the first argument is a string of length two, wraps
-- | the second argument between the first character and the
-- | second. Otherwise just returns the second argument.
{-
enclose :: [a] -> [a] -> [a]
enclose (x:y:[]) zs = [x] ++ zs ++ [y]
enclose _ zs = zs
-}

-- | Adds two spaces after all new line characters.
indent :: String -> String
indent = ("  " ++) . replace '\n' "\n  "
