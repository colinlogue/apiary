module Apiary.Parse
  ( parseApiFile
  , putParseErr
  ) where

import Data.List (intercalate)
import Text.Read (readMaybe)

import Apiary.Types
import Types
import Utils

type ParseError = [String]
type ParseResult a = Result ParseError a

parseApiFile :: String -> ParseResult API
parseApiFile = fmap API . traverse parseApiLine . lines

parseApiLine :: String -> ParseResult ApiRule
parseApiLine str =
  case words str of
    [meth, path, constructor] ->
      do
        m <- parseMethod meth
        p <- parsePath (split '/' $ tail path)
        c <- parseConstructor p constructor
        pure $ ApiRule m p c
    _ -> Err $ mkErr $
      "Unable to parse line: " ++ str ++ newline
      ++ "Lines must be in form METHOD PATH CONSTRUCTOR"

parseMethod :: String -> ParseResult StdMethod
parseMethod m = maybeToResult errMsg $ readMaybe m
  where errMsg = mkErr "Unknown HTTP method"

parsePath :: [String] -> ParseResult PathTemplate
parsePath [] = Ok EmptyPath
parsePath (('<':arg):xs) = case sliceLast arg of
  Just (x, '>') -> parsePath xs >>= Ok . ValuePath x
  _ -> Err $ mkErr $ "Missing closing bracket: <" ++ arg
parsePath (x:xs) = parsePath xs >>= Ok . ExactPath x

parseConstructor :: PathTemplate -> String -> ParseResult Constructor
parseConstructor p ctorname = getCtor [] p
    where
      getCtor ::
        [String] ->
        PathTemplate ->
        ParseResult Constructor
      getCtor as EmptyPath = Ok $ Constructor
        { ctorName = ctorname
        , args = as }
      getCtor as (ExactPath _ rest) = getCtor as rest
      getCtor as (ValuePath s rest) = getCtor (as++[s]) rest

mkErr :: String -> ParseError
mkErr = (:[])

putParseErr :: ParseError -> IO ()
putParseErr = putStrLn . (intercalate newline)

