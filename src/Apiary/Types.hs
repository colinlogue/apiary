module Apiary.Types where

import Types

data Constructor = Constructor 
  { ctorName :: String
  , args :: [String]
  }

data PathTemplate
  = EmptyPath
  | ExactPath String PathTemplate
  | ValuePath String PathTemplate

data ApiRule = ApiRule
  { method :: StdMethod
  , pathTemplate :: PathTemplate
  , ctor :: Constructor
  }

data API
  = API [ApiRule]