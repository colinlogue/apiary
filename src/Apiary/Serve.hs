module Apiary.Serve
  ( runserver
  ) where

import Network.HTTP.Types
import Network.Wai
import Network.Wai.Handler.Warp

import Apiary.Handle (handle, Handler)
import Apiary.Servable (Servable(..))
import Apiary.Types
import Types



runserver :: Servable a => Port -> [Handler a] -> IO ()
runserver port handlers = run port app
  where
    app :: Application
    app req = (respond req >>=)

    respond :: Request -> IO Response
    respond = serve . handle handlers
