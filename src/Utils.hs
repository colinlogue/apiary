module Utils where

import Data.Text (unpack)
import Network.Wai (pathInfo)

import Types

-- | Appends an element to the end of the list.
append :: a -> [a] -> [a]
append x = (++[x])

-- | Get the first element of a Maybe Pair, if there is one.
justFst :: Maybe (a, b) -> Maybe a
justFst = fmap fst

-- | Converts a Maybe to a Result, using the given error value
-- | if the Maybe value is Nothing.
maybeToResult :: err -> Maybe a -> Result err a
maybeToResult _ (Just x) = Ok x
maybeToResult e Nothing = Err e

-- | Converts a Result to a Maybe, dropping any error value.
maybeOk :: Result a b -> Maybe b
maybeOk (Ok x) = Just x
maybeOk _ = Nothing

-- | Alias for newline character.
newline :: String
newline = "\n"

-- | Extracts and unpacks a Path from a Request.
getPath :: Request -> Path
getPath = fmap Data.Text.unpack . pathInfo

-- | Replaces every occurence of a particular item with
-- | a list of items.
replace :: Eq a => a -> [a] -> [a] -> [a]
replace _ _ [] = []
replace old new (x:xs)
  | x == old = new ++ replace old new xs
  | otherwise = x : replace old new xs

-- | Safe version of `head`.
safeHead :: [a] -> Maybe a
safeHead [] = Nothing
safeHead (x:_) = Just x

-- | Safe version of `init`.
safeInit :: [a] -> Maybe [a]
safeInit [] = Nothing
safeInit (_:[]) = Just []
safeInit (x:xs) = fmap (x:) (safeInit xs)

-- | Safe version of `last`.
safeLast :: [a] -> Maybe a
safeLast [] = Nothing
safeLast (x:[]) = Just x
safeLast (_:xs) = safeLast xs

-- | Safe version of `Last`.
safeTail :: [a] -> Maybe [a]
safeTail [] = Nothing
safeTail (_:xs) = Just xs

-- | Returns a tuple of the head and tail of the list if
-- | it has at least one element.
sliceHead :: [a] -> Maybe (a, [a])
sliceHead [] = Nothing
sliceHead (x:xs) = Just (x, xs)

-- | Returns a tuple of the init and last of the list if
-- | it has at least one element.
sliceLast :: [a] -> Maybe ([a], a)
sliceLast [] = Nothing
sliceLast (x:[]) = Just ([], x)
sliceLast (x:xs) = sliceLast xs >>= \(u,v) -> Just (x:u, v)

-- | Splits a list into sublists at each occurence of a given
-- | element.
split :: Eq a => a -> [a] -> [[a]]
split x ys = split' x [] [] ys where
  split' :: Eq a => a -> [a] -> [[a]] -> [a] -> [[a]]
  split' _ cur pre [] = pre ++ [cur]
  split' u cur pre (v:vs)
    | u == v    = split' u [] (pre ++ [cur]) vs
    | otherwise = split' u (cur ++ [v]) pre vs