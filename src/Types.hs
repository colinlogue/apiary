{-# LANGUAGE PatternSynonyms #-}

module Types
  ( pattern InvalidMethod
  , pattern Err
  , pattern Ok
  , Result
  , SrcFilePath
  , DestFilePath
  , Path
  , StdMethod
  , Request
  , Response
  ) where

import Network.Wai (Request, Response)
import Network.HTTP.Types.Method (StdMethod)

pattern InvalidMethod :: Result a b
pattern InvalidMethod <- Left _

type Result = Either

pattern Err :: a -> Either a b
pattern Err x = Left x

pattern Ok :: b -> Either a b
pattern Ok y = Right y

type SrcFilePath = FilePath
type DestFilePath = FilePath

type Path = [String]
