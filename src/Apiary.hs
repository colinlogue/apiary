module Apiary where

import Language.Haskell.Parser

import Apiary.Parse (parseApiFile, putParseErr)
import Apiary.Render (render)
import Types

-- | Reads in a file describing the API behavior and renders a
-- | haskell file implementing that API at the destination.
renderApiFile :: SrcFilePath -> DestFilePath -> IO ()
renderApiFile src dest = do
  contents <- readFile src
  res <- pure (parseApiFile contents >>= Ok . render)
  case res of
    Ok x ->
      case parseModule x of
        ParseOk _ -> writeFile dest x
        ParseFailed loc msg -> putStrLn $
          msg ++ " at location " ++ show loc
    Err y -> putParseErr y
