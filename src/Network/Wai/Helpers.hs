module Network.Wai.Helpers where

import Data.Aeson
import Data.ByteString.Builder
import Network.HTTP.Types
import Network.Wai

-- Response helpers
okResponse :: [Header] -> String -> Response
okResponse hs body = responseBuilder ok200 hs $ stringUtf8 body

jsonResponse :: ToJSON a => [Header] -> a -> Response
jsonResponse hs obj = responseBuilder ok200 hs $ encodeBuilder obj

errNotFound :: Response
errNotFound = responseBuilder notFound404 [] $ stringUtf8 "not found"

encodeBuilder :: ToJSON a => a -> Builder
encodeBuilder = lazyByteString . encode
