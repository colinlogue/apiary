# Apiary

## Usage
- Render the API.hs file with renderApiFile
- Define an instance of Servable for the Action data type
  defined in the rendered API file

In your Main.hs:

- import Apiary.Serve (runserver)
- import API (handlers)
- runserver 8080 handlers (to run the server at port 8080)