module HandleTests where

import Data.List (intercalate)
import Data.Text (pack, unpack)
import Flow
import Network.Wai as Wai
import Network.HTTP.Types.Method
import Test.HUnit (Test(..), assertEqual)

import Apiary.Test.ExampleAction
import Apiary.Handle
import Types


data Expected a = Expected
  { method :: StdMethod
  , path :: Path
  , result :: a
  }

mkHandleTest ::
  Handler ExampleAction -> [Expected ExampleAction] -> Test
mkHandleTest h results = TestList $ fmap toCase results where
  toCase :: Expected ExampleAction -> Test
  toCase x = TestCase $
    assertEqual "" (result x) (getHandler h $ y) where
      y = (method x, path x)

getExHandler :: Handler ExampleAction
getExHandler = mkHandler GET GetExample match
  where match = exact "example" .> eof

putExHandler :: Handler ExampleAction
putExHandler = mkHandler PUT PutExample match
  where match = exact "example" .> value .> eof

handlers = [
  getExHandler,
  putExHandler
  ]

mkRequestTest :: (Show a, Eq a) => (Request -> a) -> [(Request, a)] -> Test
mkRequestTest f = TestList . fmap (\(req, x) -> TestCase $
  assertEqual (displayRequest req) x (f req))

mkRequest :: StdMethod -> [String] -> Request
mkRequest meth path = Wai.defaultRequest
  { Wai.requestMethod = renderStdMethod meth
  , Wai.pathInfo = fmap pack path
  }

displayRequest :: Request -> String
displayRequest req =
  (show $ requestMethod req) ++ " " ++
  (intercalate "/" $ fmap unpack $ pathInfo req)

tests = TestList [
  TestLabel "ExampleAction tests" $ TestList [
    mkHandleTest getExHandler [
      Expected
        { method = GET
        , path = []
        , result = InvalidAction
        },
      Expected
        { method = GET
        , path = ["example"]
        , result = GetExample
        },
      Expected
        { method = PUT
        , path = ["example"]
        , result = InvalidAction
        },
      Expected
        { method = GET
        , path = ["example", "more"]
        , result = InvalidAction
        }
      ],
    mkHandleTest putExHandler [
      Expected
        { method = PUT
        , path = []
        , result = InvalidAction
        },
      Expected
        { method = GET
        , path = ["example", "3"]
        , result = InvalidAction
        },
      Expected
        { method = PUT
        , path = ["example", "3"]
        , result = PutExample 3
        },
      Expected
        { method = PUT
        , path = ["example"]
        , result = InvalidAction
        },
      Expected
        { method = PUT
        , path = ["example", "three"]
        , result = InvalidAction
        }
      ]
    ],
  TestLabel "handle tests" $ TestList [
    mkRequestTest (handle handlers) [
      (mkRequest GET ["example"], GetExample),
      (mkRequest PUT ["example","3"], PutExample 3),
      (mkRequest GET ["example","3"], InvalidAction),
      (mkRequest PUT ["example"], InvalidAction),
      (mkRequest PUT ["example","three"], InvalidAction),
      (mkRequest GET [], InvalidAction)
      ]
    ]
  ]
