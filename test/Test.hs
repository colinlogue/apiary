


import HandleTests as HT (tests)
import RenderTests as RT (tests)
import Test.HUnit

main :: IO ()
main = do
  runTestTT HT.tests
  runTestTT RT.tests
  pure ()