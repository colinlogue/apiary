module Apiary.Test.ExampleAction where

import Apiary.Servable
import Network.Wai.Helpers

data ExampleAction
  = InvalidAction
  | GetExample
  | PutExample Int
  deriving (Show, Eq)

instance Servable ExampleAction where
  invalid = InvalidAction

  serve InvalidAction = pure errNotFound
  serve x = pure $ okResponse [] $ show x